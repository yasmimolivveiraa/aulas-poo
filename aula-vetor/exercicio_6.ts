/* Crie um vetor chamado " alunos " contendo três objetos, cada um representando um aluno com as seguintes propriedades: "nome ( string )" " idade " ( number ) e " notas "  ( array de números ). Preencha o vetor com informações fictícias.
Em seguida, percorra o vetor utilizando a função "forEach" e para cada aluno, calcule a média das notas e imprima o resultado na tela, juntamente com o nome e a idade do aluno. */
    
interface Aluno {
        nome: string;
        idade: number;
        notas: number [];
    }
    namespace exercicio_6{
        const alunos: Aluno[] = [
            {nome: "aluno 1", idade: 20, notas:[4,7,8]},
            {nome: "aluno 2", idade: 23, notas:[2,5,10]},
            {nome: "aluno 3", idade: 49, notas:[10,9,1]},
            {nome: "aluno 4", idade: 18, notas:[3,4,2]},
            {nome: "aluno 5", idade: 33, notas:[1,10,5]},
        ]
        
            alunos.forEach((aluno) => {
                let media = aluno.notas.reduce(
                    (total, nota) => {return total + nota}
                ) / aluno.notas.length
                if (media >= 6){
                    console.log(`A media do aluno: ${aluno.nome} é igual ${media} e está aprovado`);
                } else {
                    console.log(`A media do aluno: ${aluno.nome} é igual ${media} e está reprovado`);
                    
                }
            } )
        }

    


